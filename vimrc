" Use Vim settings, rather then Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible
set encoding=utf-8      " Needed for compatibility with powerline

" Auto-install vim plug
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall | source $MYVIMRC
endif
call plug#begin('~/.vim/plugged')
Plug 'jnurmine/zenburn'                 " Colorscheme
Plug 'altercation/vim-colors-solarized' " Colorscheme
Plug 'vim-scripts/Corporation'          " Colorscheme
Plug 'vim-scripts/Cobalt'               " Colorscheme
Plug 'vim-scripts/matchit.zip'          " Matches brackets
Plug 'nathanaelkane/vim-indent-guides'  " Shows indents visually, '\ig'
Plug 'tpope/vim-surround'               " quoting/parenthesizing made easy
Plug 'easymotion/vim-easymotion'        " makes it fast to move around '\\w'
Plug 'scrooloose/nerdtree'              " Tree explorer
Plug 'Xuyuanp/nerdtree-git-plugin'      " Git status in Nerdtree
Plug 'tomtom/tcomment_vim'              " Comment plugin
Plug 'tpope/vim-fugitive'               " GIT wrapper for VIM
Plug 'ctrlpvim/ctrlp.vim'               " file search
Plug 'vim-airline/vim-airline' | Plug 'vim-airline/vim-airline-themes'
Plug 'sjl/gundo.vim'                    " Graphical undo tree
Plug 'ryanoasis/vim-devicons'           " Adds cool little icons
Plug 'SirVer/ultisnips'| Plug 'honza/vim-snippets'
Plug 'Valloric/YouCompleteMe', { 'do': './install.py --clang-completer' }
Plug 'maralla/completor.vim'            " Autocomplete support
Plug 'tpope/vim-dispatch'               " Launch make in separate process (:Make)
Plug 'majutsushi/tagbar'                " Show tags for current file, 'F8'
Plug 'airblade/vim-gitgutter'           " Shows GIT line status in gutter
Plug 'asenac/vim-opengrok'              " OpenGrok integration
Plug 'szw/vim-ctrlspace'                " Fast/fuzzy file searching
Plug 'will133/vim-dirdiff'              " Diff two directories
Plug 'chazy/cscope_maps'                " Mirror of http://cscope.sourceforge.net/cscope_maps.vim
Plug 'herringtondarkholme/yats.vim'     " Typescript support
Plug 'tmux-plugins/vim-tmux'            " Support for editing .tmux.conf files
" Plug 'HarnoRanaivo/vim-mipssyntax'      " Support for MIPS assembly
" Plug 'edkolev/tmuxline.vim'             " Support for creating tmuxline config files
call plug#end()

if has('gui_running')
  if has('win32')
    " set guifont=AnonymicePowerline_NF:h11:cANSI:qDRAFT
    " set guifont=MesloLGS_NF:h11:cANSI:qDRAFT
    " set guifont=FuraCode_NF:h11:cANSI:qDRAFT
    " set guifont=SauceCodePro_NF:h11:cANSI:qDRAFT
    set guifont=InconsolataForPowerline_NF:h12:cANSI:qDRAFT
  elseif has('mac')
    " set guifont=FiraCode-Regular:h13
    set guifont=Sauce\ Code\ Pro\ Nerd\ Font\ Complete\ Mono\ Windows\ Compatible:h13
  else
    set guifont=InconsolataForPowerline\ Nerd\ Font\ Medium\ 11
  endif

  function! ScreenFilename()
    if has('amiga')
      return "s:.vimsize"
    elseif has('win32')
      return $HOME.'\vimfiles\_vimsize'
    else
      return $HOME.'/.vimsize'
    endif
  endfunction

  function! ScreenRestore()
    " Restore window size (columns and lines) and position
    " from values stored in vimsize file.
    " Must set font first so columns and lines are based on font size.
    let f = ScreenFilename()
    if has("gui_running") && g:screen_size_restore_pos && filereadable(f)
      let vim_instance = (g:screen_size_by_vim_instance==1?(v:servername):'GVIM')
      for line in readfile(f)
        let sizepos = split(line)
        if len(sizepos) == 5 && sizepos[0] == vim_instance
          silent! execute "set columns=".sizepos[1]." lines=".sizepos[2]
          silent! execute "winpos ".sizepos[3]." ".sizepos[4]
          return
        endif
      endfor
    endif
  endfunction

  function! ScreenSave()
    " Save window size and position.
    if has("gui_running") && g:screen_size_restore_pos
      let vim_instance = (g:screen_size_by_vim_instance==1?(v:servername):'GVIM')
      let data = vim_instance . ' ' . &columns . ' ' . &lines . ' ' .
            \ (getwinposx()<0?0:getwinposx()) . ' ' .
            \ (getwinposy()<0?0:getwinposy())
      let f = ScreenFilename()
      if filereadable(f)
        let lines = readfile(f)
        call filter(lines, "v:val !~ '^" . vim_instance . "\\>'")
        call add(lines, data)
      else
        let lines = [data]
      endif
      call writefile(lines, f)
    endif
  endfunction

  if !exists('g:screen_size_restore_pos')
    let g:screen_size_restore_pos = 1
  endif
  if !exists('g:screen_size_by_vim_instance')
    let g:screen_size_by_vim_instance = 1
  endif
  autocmd VimEnter * if g:screen_size_restore_pos == 1 | call ScreenRestore() | endif
  autocmd VimLeavePre * if g:screen_size_restore_pos == 1 | call ScreenSave() | endif
endif

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

set history=50		" keep 50 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set incsearch		" do incremental searching
set smartcase           " ignore case when doing searches
set number              " show line numbers
set wmh=0               " do not display line of min window
set mouse=vn            " enable mouse in visual and normal mode
set hidden              " hide a modified buffer, rather than prompting
set updatetime=750      " This helps plugins like gitgutter to update faster
set nowrap              " Don't wrap long lines
set cursorline          " Highlight current line

" Setup undo
set undofile            " preserve undo operations between vim sessions
set undodir=$HOME/.vim/vimundo
if !isdirectory(&undodir)
  call mkdir(&undodir)
endif
set nobackup            " don't need backup files

" Default tabs and tabstops
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab

" Search and match behavior
set showmatch           " highlight matching braces and brackets
set ruler               " show line / column number
set nohls               " remove highlight when search is complete
set incsearch           " search as you type

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

" Support the :Man function
runtime! ftplugin/man.vim
set keywordprg=:Man     " Map K to Man

" Move between vim windows using ctrl-w and standard vim movement keys
map <C-J> <C-W>j<C-W>_
map <C-K> <C-W>k<C-W>_

" Don't use Ex mode, use Q for formatting
map Q gq

" FUNCTION KEY MAPPINGS

" Quick switch when copy/pasting code from outside vim
nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>
" Grep for word under cursor in current and sub directories
map <F4> :execute "vimgrep /" . expand("<cword>") . "/j **/*.c"<Bar>cw<CR>
" Show Gundo
nnoremap <F5> :GundoToggle<CR>
" Show tagbar
nmap <F8> :TagbarToggle<CR>

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch
  if !has("gui_running")
    set t_Co=256          " Force VIM to 256 color mode
  endif
endif

" Diff mode behavior
if &diff
  " Better highlight code that has changed
  highlight! link DiffText MatchParen
endif
set diffopt=vertical,filler

" Create file if it doesn't exist with gF keystroke
nnoremap gF :view <cfile><cr>

" Only do this part when compiled with support for autocommands.
if has("autocmd")
  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
    au!
    au FileType javascript setl sw=2 sts=2
    au FileType html setl sw=2 sts=2
    au FileType typescript setl sw=2 sts=2
    au FileType css setl sw=2 sts=2
    au FileType vim setl sw=2 sts=2

    " For all text files set 'textwidth' to 78 characters.
    autocmd FileType text setlocal textwidth=78

    " When editing a file, always jump to the last known cursor position.
    " Don't do it when the position is invalid or when inside an event handler
    " (happens when dropping a file on gvim).
    autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \   exe "normal g`\"" |
    \ endif
  augroup END

  augroup filetypedetect
    au BufRead,BufNewFile *.inc setfiletype make
    " associate *.foo with php filetype
  augroup END
else
  set autoindent		" always set autoindenting on
endif " has("autocmd")

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
                  \ | wincmd p | diffthis
endif

" Exit from insert mode using "jk", rather than having to hit ESC
inoremap jk <Esc>

set background=dark
colorschem zenburn

" Mark column 80 and 120 with darker color
highlight ColorColumn ctermbg=235 guibg=#2c2d27
" let &colorcolumn="80,".join(range(120,999),",")
let &colorcolumn="80"

let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"
let g:UltiSnipsListSnippets="<c-e>"
let g:UltiSnipsEditSplit="vertical"

let g:ctrlp_regexp = 1
let g:gundo_prefer_python3 = 1

if executable("ag")
  let g:CtrlSpaceGlobCommand = 'ag -l --nocolor -g ""'
endif

set laststatus=2                " Always display the statusline in all windows
set noshowmode                  " Hide the default mode text
                                " (e.g. -- INSERT -- below the statusline)
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme="cool"

" YCM Shortcuts and configuration
let g:ycm_key_list_stop_completion = ['<C-y>', '<Enter>']
nnoremap <leader>gl :YcmCompleter GoToDeclaration<CR>
nnoremap <leader>gf :YcmCompleter GoToDefinition<CR>
nnoremap <leader>gg :YcmCompleter GoToDefinitionElseDeclaration<CR>

" You can convert a gcc VERBOSE=y command line output to YCM ready format
" with the following function:
function! VerboseToYCM()
    " separate each flag to new line:
    %s/ /
/g
    " llvm requires a space between -I and the include paths:
    %s/\(-I\)\(.*\)/'\1',
'\2',/g
    " all other definitions get processed as-is
    %s/^-\(.*\)/'-\1',/g
endfunction

" Search for cscope database in parent directories
function! LoadCscope()
    let db = findfile("cscope.out", ".;")
    if (!empty(db))
        let path = strpart(db, 0, match(db, "/cscope.out$"))
        set nocscopeverbose " suppress 'duplicate connection' error
        exe "cs add " . db . " " . path
        set cscopeverbose
    endif
endfunction
au BufEnter /* call LoadCscope()

set tags=./tags,tags;   " Search current directory for ctags

if has('unix')
  let hostname = substitute(system('hostname -s'), '\n', '', '')
  if hostname == "stbsdo-bld-1"
    " vim-opengrok (manual install)
    let g:opengrok_jar = '/local/opengrok/lib/opengrok.jar'
    let g:opengrok_ctags = '/usr/bin/ctags'
  endif
endif

map <C-x> <Plug>CompletorCppJumpToPlaceholder
imap <C-x> <Plug>CompletorCppJumpToPlaceholder

" Use TAB to complete when typing words, else inserts TABs as usual.  Uses
" dictionary, source files, and completor to find matching words to complete.

" Note: usual completion is on <C-n> but more trouble to press all the time.
" Never type the same word twice and maybe learn a new spellings!
" Use the Linux dictionary when spelling is in doubt.
function! Tab_Or_Complete() abort
  " If completor is already open the `tab` cycles through suggested completions.
  if pumvisible()
    return "\<C-N>"
  " If completor is not open and we are in the middle of typing a word then
  " `tab` opens completor menu.
  elseif col('.')>1 && strpart( getline('.'), col('.')-2, 3 ) =~ '^\w'
    return "\<C-R>=completor#do('complete')\<CR>"
  else
    " If we aren't typing a word and we press `tab` simply do the normal `tab`
    " action.
    return "\<Tab>"
  endif
endfunction

" Use `tab` key to select completions.  Default is arrow keys.
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" Use tab to trigger auto completion.  Default suggests completions as you type.
" let g:completor_auto_trigger = 0
inoremap <expr> <Tab> Tab_Or_Complete()

let g:airline_left_sep = "\uE0C6"
let g:airline_right_sep = "\uE0C7"

set fillchars=vert:│

let g:tmuxline_separators = {
  \ 'left' : "\uE0C6",
  \ 'left_alt' : "-",
  \ 'right' : "\uE0C7",
  \ 'right_alt': "\uE0D0",
  \ 'space' : " " }


" Change cursor shape when in insert mode
if exists('$TMUX')
  let &t_SI = "\ePtmux;\e\e[5 q\e\\"
  let &t_EI = "\ePtmux;\e\e[2 q\e\\"
else
  let &t_SI = "\e[5 q"
  let &t_EI = "\e[2 q"
endif

" Start NERDTree automatically if vim is started without specifying a file
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
